import React, { useEffect, useState } from "react";
import { addJob, setJobById } from "../../Services/jobs.service";
import { debounceTime, first, map, switchMap, takeUntil } from "rxjs/operators";

import Button from "./Button";
import Dropdown from "./Dropdown";
import { Job } from "../../Jobs/Jobs";
import ReactDOM from "react-dom";
import Search from "./Search";
import { SearchTextEvent$ } from "../Events/SearchText";
import { StatesList$ } from "../../Services/states.service";
import { Subject } from "rxjs";

enum DialogTypeEnum {
  primary = "primary",
  secondary = "secondary",
}
type DialogType = DialogTypeEnum.primary | DialogTypeEnum.secondary | string;

interface DialogProps {
  title: string;
  subtitle: string;
  container: Element | DocumentFragment;
  data?: Job;
  class?: DialogType;
  onClick?: () => any;
}

const Dialog = (props: DialogProps) => {
  const [dropdown, setDropdown] = useState<any[]>([]);
  // scoped subject to close the dialog
  let className: string;
  // default the Dialog class to primary
  if (!!!props.class) {
    className = DialogTypeEnum.primary;
  } else {
    className = props.class;
  }
  className = "dialog-container-" + className;

  useEffect(() => {
    const unsubscribe$ = new Subject();
    const filterResults$ = (eventString: string) =>
      StatesList$.pipe(
        // there most likely a better way of doing this
        // the data structure here is a little non-intuitive
        map((results: { [key: string]: string }[]) => {
          const searchString = eventString.toLocaleUpperCase();
          return results.map((item) => {
            const keys = Object.keys(item).filter(
              (key) => key.indexOf(searchString) !== -1
            );
            const values = Object.values(item).filter(
              (val) => val.toLocaleUpperCase().indexOf(searchString) !== -1
            );
            // We really only care if there are matches found
            if (keys.length || values.length) {
              return item;
            }
            return null;
          });
        }),
        map((values) => values.filter((val) => val !== null)),
        first()
      );

    const search$ = SearchTextEvent$.pipe(
      debounceTime(200),
      // when the search event is emitted then switch to filter the list of results
      switchMap((event) => filterResults$(event.value)),
      // needed to bolt on the unsubscribe here
      takeUntil(unsubscribe$)
    );
    search$.subscribe(setDropdown);

    // just grab the first item
    const elem = document
      .getElementsByClassName(className)
      .item(0) as HTMLElement;
    if (elem) {
      elem.focus();
    }
    return () => unsubscribe$.unsubscribe();
  }, [props, className]);

  const closeDialog = () => {
    ReactDOM.unmountComponentAtNode(props.container);
  };

  const renderForm = (job: Job | undefined) => {
    return (
      <form autoComplete="off">
        {/* Job Title */}
        <div className="input-wrapper">
          <label aria-label="Job Title" label-for="jobTitle">
            Job Title
          </label>
          <p>What is name of the role?</p>
          <input
            id="jobTitle"
            name="jobTitle"
            type="text"
            placeholder="e.g. Software Engineer"
            defaultValue={job?.title}
          />
        </div>

        {/* Location */}
        <div className="input-wrapper">
          <label aria-label="Location" label-for="jobLocation">
            Location
          </label>
          <p>Where is this job?</p>
          {/* <input
            id="jobLocation"
            name="jobLocation"
            type="text"
            placeholder="e.g. Chicago, IL"
            defaultValue={getStateByAbbrev(job?.location || '')}
          /> */}
          <div className="type-ahead-search">
            <Search name="job-search" placeholder="Type TX or Texas" />
            {/* // Didn't have time to finish this */}
            <Dropdown items={dropdown} />
          </div>
        </div>

        {/* Sponsorship */}
        <div className="input-wrapper">
          <label aria-label="Sponsorship" label-for="jobSponsorship">
            Sponsorship
          </label>
          <p>Do you want to promote this job?</p>
          <select
            id="jobSponsorship"
            name="jobSponsorship"
            defaultValue={job?.sponsorship}
          >
            {/* 
              // hardcoded this to make it easy =)  
              // Warning: Each child in a list should have a unique "key" prop
              */}
            {["Free", "Sponsorship"].map((item) => (
              <option value={item}>{item}</option>
            ))}
          </select>
        </div>

        {/* Status */}
        <div className="input-wrapper">
          <label aria-label="Status" label-for="jobStatus">
            Status
          </label>
          <p>Do you want to promote this job?</p>
          <select id="jobStatus" name="jobStatus" defaultValue={job?.status}>
            {/* 
              // hardcoded this to make it easy =)  
              // Warning: Each child in a list should have a unique "key" prop
              */}
            {["Closed", "Open", "Paused"].map((item) => (
              <option value={item}>{item}</option>
            ))}
          </select>
        </div>
      </form>
    );
  };

  // shortcut
  const submitBtnText = props.data ? "Save" : "Add Job";
  const submit = () => {
    // this would be better as it's own function call
    // also if I knew how to use React
    const data = props.data as Job;
    if (submitBtnText === "Save") {
      setJobById({
        ...data,
        datePosted: "05/02/2021",
      });
      // closeDialog();
    } else if (submitBtnText === "Add Job") {
      addJob({
        id: 111111111, // needs to be IRL
        company: "New Age Of Old School",
        datePosted: "05/02/2021",
        isFullTime: true,
        location: "Wheresville, 007",
        salary: 300000,
        salaryType: "ADA",
        sponsorship: "Free",
        status: "Open",
        title: "Beast Master",
      });
    }
    closeDialog();
  };
  return (
    <React.Fragment>
      <div className="overlay"></div>
      <section
        className={className}
        tabIndex={1}
        role="dialog"
        aria-labelledby="dialog-title"
        aria-describedby="dialog-desc"
      >
        <div className="dialog-heading">
          <h2 id="dialog-title">{props.title}</h2>
          <p id="dialog-desc">{props.subtitle}</p>
        </div>
        <section className="dialog-content">
          {/* // insert content here */}
          {renderForm(props?.data)}
        </section>
        <div className="dialog-footer">
          <div>
            <Button text="Cancel" class="secondary" onClick={closeDialog} />

            <Button text={submitBtnText} onClick={submit} />
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default Dialog;
