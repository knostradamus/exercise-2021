import React from "react";

enum ButtonTypeEnum {
  primary = "primary",
  secondary = "secondary",
}
type ButtonType = ButtonTypeEnum.primary | ButtonTypeEnum.secondary | string;

interface ButtonProps {
  text: string;
  class?: ButtonType;
  onClick?: () => any;
}

const Button = (props: ButtonProps) => {
  let className;
  // default the button class to primary
  if (!!!props.class) {
    className = ButtonTypeEnum.primary;
  } else {
    className = props.class;
  }
  const defaultAction = () => {
    console.log("default btn click");
  };
  return (
    <button className={className} onClick={props?.onClick || defaultAction}>
      {props.text}
    </button>
  );
};

export default Button;
