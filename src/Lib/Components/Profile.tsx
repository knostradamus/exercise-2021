import React from 'react';

interface ProfileProps {
  name: string;
  role: string;
}

const Profile = (props: ProfileProps) => {
  return (
    <div className="profile">
      <div>
        <div className="img"></div>
      </div>
      <div className="user-profile">
        <div className="user">{props.name}</div>
        <div className="role">{props.role}</div>
      </div>
    </div>
  );
};

export default Profile;
