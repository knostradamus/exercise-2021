import React, { useEffect, useState } from 'react';

interface DropdownProps {
  items: any[];
  onClick?: () => any;
}

const Dropdown = (props: DropdownProps) => {
  const [state, setState] = useState<any[]>([]);

  useEffect(() => {
    setState(props.items);
  }, [props]);

  const returnItem = (item: string) => {    
    return <li>{JSON.stringify(item)}</li>;
  };

  if (props.items.length) {
    return (
      <ol>
        {state?.map((item, _idx) => (
          <React.Fragment key="{item.id}">{returnItem(item)}</React.Fragment>
        ))}
      </ol>
    );
  } else return <React.Fragment></React.Fragment>;
};

export default Dropdown;
