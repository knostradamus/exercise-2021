import { Subject, of } from 'rxjs';
import { debounceTime, filter, map, mergeMap, tap } from 'rxjs/operators';
import { useEffect, useState } from 'react';

import { emitSearchText } from '../Events/SearchText';

interface SearchProps {
  name: string; // TODO: Make this unique
  placeholder: string;
}

type Action = 'onChange';

interface SearchAction {
  action: Action;
  event: Event;
  name: string;
}

const action$ = new Subject<SearchAction>();
const typeAction$ = action$.pipe(
  filter((action) => action.action === 'onChange'),
  debounceTime(300),
  mergeMap((action) => of(action.event).pipe(map((e: any) => e.target.value))),
  tap((value) => emitSearchText(value))
);

const Search = (props: SearchProps) => {
  const [name, setName] = useState('__search');
  const [placeholder, setPlaceholder] = useState('__placeholder');
  const [, setSearch] = useState();

  const handleInputChange = (event: any) => {
    if (event.target) {
      action$.next({ action: 'onChange', event: event, name: name });
    }
  };

  useEffect(() => {
    const subscriber = typeAction$.subscribe(setSearch);
    setName(props.name);
    setPlaceholder(props.placeholder);
    return () => subscriber.unsubscribe();
  }, [props.name, props.placeholder]);

  return <input name={name} type="search" autoComplete="off" placeholder={placeholder} onChange={handleInputChange} />;
};

export default Search;
