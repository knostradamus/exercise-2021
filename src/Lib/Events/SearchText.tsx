import { Observable, Subject } from 'rxjs';

import { map } from 'rxjs/operators';

export interface EventValue {
  event: string;
  value: any;
}
// constant subject
const SearchText$ = new Subject();
// trigger-able event
export const emitSearchText = (txt: string) => {
    SearchText$.next(txt);
}
// subscribable event stream 
export const SearchTextEvent$: Observable<EventValue> = SearchText$.pipe(map((txt) => ({ event: 'SEARCH', value: txt })));
