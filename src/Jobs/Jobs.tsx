import React, { useEffect, useState } from 'react';

import Button from '../Lib/Components/Button';
import Dialog from '../Lib/Components/Dialog';
import ReactDOM from 'react-dom';
import { getStateByAbbrev } from '../Services/states.service';

enum Moneys {
  Bitcoin = 'BTC',
  Cardano = 'ADA',
  Etherium = 'ETH',
  USDCoin = 'USDC'
}

type SalaryType = Moneys.Bitcoin | Moneys.Cardano | Moneys.Etherium | Moneys.USDCoin | string;

enum JobStatusEnum {
  c = 'Closed',
  o = 'Open',
  p = 'Paused'
}

type JobStatusType = JobStatusEnum.c | JobStatusEnum.o | JobStatusEnum.p | string;

export interface Job {
  id: number;
  company: string;
  datePosted: string;
  isFullTime: boolean;
  location: string;
  salary: number;
  salaryType: SalaryType;
  sponsorship: 'Free' | 'Sponsored';
  status: JobStatusType;
  title: string;
}

export interface JobsProps {
  jobList: Job[];
}

// Dialog -ish
const dialogEditJob = 'dialog-edit-job';

// had to use any here because I don't know React
const openDialog: any = (job: Job) => {
  if (job) {
    const container: Element | any = document.getElementById(dialogEditJob);
    return ReactDOM.render(
      <Dialog data={job} container={container} title="Edit job" subtitle="Edit the information for your job listing." />,
      container
    );
  }
  return null;
};

const Jobs = (props: JobsProps) => {
  const [jobs, setJobs] = useState<Job[]>([]);

  useEffect(() => {
    setJobs(props.jobList);
  }, [props]);

  const getJobElement = (job: Job) => {
    return (
      <div className="table-row">
        <div className="table-cell">
          {job.title}
          <small className="muted">{getStateByAbbrev(job.location)}</small>
        </div>
        <div className="table-cell">{job.datePosted}</div>
        <div className="table-cell">{job.sponsorship}</div>
        <div className="table-cell">{job.status}</div>
        <div className="table-cell">
          <Button text="Edit" class="secondary" onClick={() => openDialog(job)} />
        </div>
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="table">
        <div className="table-row-header">
          <div className="table-cell">Job Title</div>
          <div className="table-cell">Posted</div>
          <div className="table-cell">Sponsorship</div>
          <div className="table-cell">Status</div>
          <div className="table-cell">{/* // actions */}</div>
        </div>
        {jobs?.map((job, _idx) => getJobElement(job))}
      </div>
      <section id={dialogEditJob}>{/* // something will go here */}</section>
    </React.Fragment>
  );
};

export default Jobs;
