import { BehaviorSubject, Observable, Subject, merge } from 'rxjs';
import { map, tap, withLatestFrom } from 'rxjs/operators';

import { Job } from '../Jobs/Jobs';

const JOB_DATA: Job[] = [
  {
    id: 9912223,
    company: 'Darth Ventures',
    datePosted: '02/24/2021',
    isFullTime: false,
    location: 'TX',
    salary: 1000,
    salaryType: 'ADA',
    sponsorship: 'Sponsored',
    status: 'Open',
    title: 'Technical Associate Tertiary Assistant (Intern)'
  },
  {
    id: 9912224,
    company: 'Blockchain Madness',
    datePosted: '03/04/2021',

    isFullTime: true,
    location: 'WA',
    salary: 4,
    salaryType: 'BTC',
    sponsorship: 'Free',
    status: 'Open',
    title: 'Smart Contract Engineer'
  },
  {
    id: 9912225,
    company: 'Valued Company',
    datePosted: '01/12/2021',
    isFullTime: true,
    location: 'TN',
    salary: 200000,
    salaryType: 'USDC',
    sponsorship: 'Free',
    status: 'Open',
    title: 'TypeScript Developer'
  },
  {
    id: 9912226,
    company: 'Undervalued Company',
    datePosted: '12/02/2020',
    isFullTime: true,
    location: 'VA',
    salary: 200000,
    salaryType: 'USDC',
    sponsorship: 'Sponsored',
    status: 'Open',
    title: 'Development Manager'
  }
];
// enforce immutability by providing the observable converted from a behavior subject
const JOB_LIST$ = new BehaviorSubject<Job[]>(JOB_DATA);
const actionSetJobById$ = new Subject<Job>();
const actionAddJob$ = new Subject<Job>();

// overwrite the old Job with the new info
const SetJobById$ = actionSetJobById$.pipe(
  withLatestFrom(JOB_LIST$),
  map(([updatedJob, previousJobs]) => ({ updatedJob, previousJobs })),
  map((state) => [...state.previousJobs.filter((job: Job) => job.id !== state.updatedJob.id), state.updatedJob])
);

// append a new job to the list
const AddJob$ = actionAddJob$.pipe(
  withLatestFrom(JOB_LIST$),
  map(([updatedJob, previousJobs]) => ({ updatedJob, previousJobs })),
  map((state) => [...state.previousJobs, state.updatedJob])
);

const Actions$ = merge(SetJobById$, AddJob$).pipe(tap((state) => JOB_LIST$.next(state)));

// export the behavior subject as an observable
export const JobList$: Observable<Job[]> = merge(JOB_LIST$, Actions$);

export const setJobById = (job: Job) => {
  actionSetJobById$.next(job);
};

export const addJob = (job: Job) => {
    actionAddJob$.next(job);
  };
