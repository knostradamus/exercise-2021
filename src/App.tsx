import "./App.scss";

import Jobs, { Job } from "./Jobs/Jobs";
import React, { useEffect, useState } from "react";

import Button from "./Lib/Components/Button";
import Dialog from "./Lib/Components/Dialog";
import { JobList$ } from "./Services/jobs.service";
import Profile from "./Lib/Components/Profile";
import ReactDOM from "react-dom";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// our main subject to be used to unsubscribe on un-mount
const unsubscribe$ = new Subject();
// main data observables
// pass in unsubscribe here to handle component un-mount
const jobList$ = JobList$.pipe(takeUntil(unsubscribe$));
function App() {
  // state list jobs and dropdown results from text search
  const [jobs, setJobs] = useState<Job[]>([]);

  useEffect(() => {
    // subscribe to new values and set state ... I think
    jobList$.subscribe(setJobs);
    // return function for un-mounting component
    return () => unsubscribe$.unsubscribe();
  }, []);

  /**
   * Dialog Instance for new jobs
   */
  const dialogNewJob = "dialog-new-job";
  const openDialog = () => {
    // had to use any here because I don't know React
    const container: Element | any = document.getElementById(dialogNewJob);
    return ReactDOM.render(
      <Dialog container={container} title="Add a new job" subtitle="Fill out the information for your new job listing." />,
      container
    );
  };

  return (
    <div className="App">
      <header className="header">
        {/* // profile area */}
        <Profile name="Milton" role="Dragon Slayer" />
      </header>
      {/* // main area */}
      <main id="main">
        <section className="heading">
          <h1>
            Jobs
            <small className="muted">
              {jobs.length + ' listings'}               
            </small>
          </h1>
          <div className="actions">
            {/* {openDialog} */}
            <Button text="Add Job" onClick={openDialog} />
          </div>
        </section>
        {/* {jobs} */}
        <Jobs jobList={jobs} />
      </main>
      {/* // DIALOG INJECTION AREA */}
      <section id={dialogNewJob}></section>
    </div>
  );
}

export default App;
