# README #

This is an example project that shows a list of jobs with simple CRUD -- without DELETE -- operations. I've even put in a type ahead search but wasn't able to complete it. =( 
    
    Things To Note:
    
    This is my first React App
    I did try and implement tests
    There may be anti-patterns
    I didn't spend much time on this
    This is my first React App 

### What my goal was get these working ###

* Jobs list - Display a list of job postings and their attributes
* Create / Edit job - A page to create or edit a job post

### I wanted to ...  ###
* Learn about React
* Learn about Flexbox
* Learn about SCSS (*it's been a while*)
* Write tests
* Also, tried to initially create a public bitbucket repo and failed. This is the clone from that repo 
